#include "msg_queue.h"
#include <stdint.h>

static uint32_t local_strcpy(char *dest, char *src, uint32_t max_length)
{
    uint32_t i;
    for (i = 0; i < max_length; ++i)
    {
        dest[i] = src[i];
        if (src[i] == '\0')
        {
            break;
        }
    }
    return i;
}

static uint32_t local_strlen(char *str)
{
    uint32_t i;
    for (i = 0; str[i] != '\0'; ++i);
    return i;
}

uint8_t msg_queue_init(t_msg_queue *q)
{
    // Initialize the queue with 0
    q->begin = 0;
    q->end = 0;
    q->size = 0;
    // Fill the queue with empty messages
    for (uint32_t i = 0; i < MSG_QUEUE_SIZE; ++i)
    {
        for (uint32_t j = 0; j < MSG_MAX_LENGTH; ++j)
        {
            q->msg[i].data[j] = 0;
        }
    }
    return MSG_QUEUE_NO_ERROR;
}

uint8_t msg_queue_push(t_msg_queue *q, char * msg)
{
    // Check if the queue is full
    if (local_strlen(msg) > MSG_MAX_LENGTH)
    {
        return MSG_QUEUE_ERROR_LENGTH;
    }
    // If the queue is full, return an error
    if (q->size == MSG_QUEUE_SIZE)
        return MSG_QUEUE_ERROR_FULL;
    // Copy the message to the queue
    local_strcpy(q->msg[q->end].data, msg, MSG_MAX_LENGTH);
    // Increment the end index
    q->end = (q->end + 1) % MSG_QUEUE_SIZE;
    // Increment the size
    q->size++;
    return MSG_QUEUE_NO_ERROR;
}

uint8_t msg_queue_pop(t_msg_queue *q, char *msg)
{
    // Check if the queue is empty
    if (q->size == 0)
        return MSG_QUEUE_ERROR_EMPTY;
    // Copy the message from the queue
    local_strcpy(msg, q->msg[q->begin].data, MSG_MAX_LENGTH);
    // Clear the message
    for (uint32_t i = 0; i < MSG_MAX_LENGTH; ++i)
    {
        q->msg[q->begin].data[i] = 0;
    }
    // Increment the begin index
    q->begin = (q->begin + 1) % MSG_QUEUE_SIZE;
    // Decrement the size
    q->size--;
    return MSG_QUEUE_NO_ERROR;
}

uint8_t msg_queue_is_empty(t_msg_queue *q)
{
    // Check if the queue is empty
    if (q->size == 0)
        return 1;
    else
        return 0;
}

uint8_t msg_queue_is_full(t_msg_queue *q)
{
    // Check if the queue is full
    if (q->size == MSG_QUEUE_SIZE)
        return 1;
    else
        return 0;
}
