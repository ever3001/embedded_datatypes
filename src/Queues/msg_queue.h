#ifndef __MSG_QUEUE_H__
#define __MSG_QUEUE_H__


#include <stdint.h>

#define MSG_QUEUE_SIZE 10
#define MSG_MAX_LENGTH 64

typedef struct 
{
    char data[MSG_MAX_LENGTH];
} t_msg;

typedef struct 
{
    t_msg msg [MSG_QUEUE_SIZE];
    uint8_t begin;
    uint8_t end;
    uint32_t current_load;
    uint8_t size;
} t_msg_queue;

typedef enum
{
    MSG_QUEUE_NO_ERROR = 0,
    MSG_QUEUE_ERROR_FULL,
    MSG_QUEUE_ERROR_EMPTY,
    MSG_QUEUE_ERROR_LENGTH
} t_msg_queue_error;

extern t_msg_queue v_msg_queue;

/**
* @brief        Initialize the message queue
*
* @param[in]    q: pointer to the message queue
*
* @return       MSG_QUEUE_NO_ERROR if no error
*/ 
uint8_t msg_queue_init(t_msg_queue *q);

/**
* @brief        Push a message in the queue
*
* @param[in]    q: pointer to the message queue
* @param[in]    msg: pointer to the message to push
*
* @return       MSG_QUEUE_NO_ERROR if no error
*/
uint8_t msg_queue_push(t_msg_queue *q, char * msg);

/**
* @brief        Pop a message from the queue
*
* @param[in]    q: pointer to the message queue
* @param[out]   msg: pointer to the message to pop
*
* @return       MSG_QUEUE_NO_ERROR if no error
*/
uint8_t msg_queue_pop(t_msg_queue *q, char *msg);

/**
* @brief        Test if the queue is empty
*
* @param[in]    q: pointer to the message queue
*
* @return       1 if the queue is empty, 0 otherwise        
*/
uint8_t msg_queue_is_empty(t_msg_queue *q);

/**
* @brief        Test if the queue is full
*
* @param[in]    q: pointer to the message queue
*
* @return       1 if the queue is full, 0 otherwise        
*/
uint8_t msg_queue_is_full(t_msg_queue *q);

uint32_t msg_queue_size(t_msg_queue *q)

#endif // __MSG_QUEUE_H__